﻿using NETProjectTutorial.entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.model
{
    class EmpleadoModel
    {
        private static List<Empleado> ListEmpleado = new List<Empleado>();
        private implements.DaoImplementsEmpleado daoEmpleado;

        public EmpleadoModel()
        {
            daoEmpleado = new implements.DaoImplementsEmpleado();
        }

        public List<Empleado> GetListEmpleado()
        {
            return daoEmpleado.findAll();
        }

        public void Populate()
        {
            ListEmpleado = JsonConvert.DeserializeObject<List<Empleado>>(System.Text.Encoding.Default.GetString(NETProjectTutorial.Properties.Resources.Empleado_data));

            foreach (Empleado emp in ListEmpleado)
            {
                daoEmpleado.save(emp);
            }
        }

        public void save(DataRow empl)
        {
            Empleado e = new Empleado();
            e.Id = Convert.ToInt32(empl["Id"].ToString());
            e.Inss = empl["INSS"].ToString();
            e.Cedula = empl["Cédula"].ToString();
            e.Nombre = empl["Nombre"].ToString();
            e.Apellidos = empl["Apellidos"].ToString();
            e.Direccion = empl["Dirección"].ToString();
            e.Tconvencional = empl["Teléfono"].ToString();
            e.Tcelular = empl["Celular"].ToString();
            e.Sexo = (Empleado.SEXO)Enum.Parse(typeof(Empleado.SEXO), empl["Sexo"].ToString(), true);

            daoEmpleado.save(e);
        }

        public void update(DataRow empl)
        {
            Empleado e = new Empleado();
            e.Id = Convert.ToInt32(empl["Id"].ToString());
            e.Inss = empl["INSS"].ToString();
            e.Cedula = empl["Cédula"].ToString();
            e.Nombre = empl["Nombre"].ToString();
            e.Apellidos = empl["Apellidos"].ToString();
            e.Direccion = empl["Dirección"].ToString();
            e.Tconvencional = empl["Teléfono"].ToString();
            e.Tcelular = empl["Celular"].ToString();
            e.Sexo = (Empleado.SEXO)Enum.Parse(typeof(Empleado.SEXO), empl["Sexo"].ToString(), true);

            daoEmpleado.update(e);
        }

        public void delete(DataRow empl)
        {
            Empleado e = new Empleado();
            e.Id = Convert.ToInt32(empl["Id"].ToString());
            e.Inss = empl["INSS"].ToString();
            e.Cedula = empl["Cédula"].ToString();
            e.Nombre = empl["Nombre"].ToString();
            e.Apellidos = empl["Apellidos"].ToString();
            e.Direccion = empl["Dirección"].ToString();
            e.Tconvencional = empl["Teléfono"].ToString();
            e.Tcelular = empl["Celular"].ToString();
            e.Sexo = (Empleado.SEXO)Enum.Parse(typeof(Empleado.SEXO), empl["Sexo"].ToString(), true);

            daoEmpleado.delete(e);
        }
    }
}
